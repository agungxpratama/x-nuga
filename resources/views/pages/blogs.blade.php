@extends('template.template')
@section('content')
<div class="col-lg-12 d-flex align-items-stretch">
    <div class="contact-wrap w-100 p-md-5 p-4">
		<h3 class="mb-4 heading">Blogs</h3>
	</div>
</div>
@php
	$no = 0;
@endphp
@foreach ($blogs as $b)
@php
$no++;
@endphp	
@if ($no % 2 == 1)
<div class="col-md-12 portfolio-wrap">
	<div class="row no-gutters align-items-center">
		<a href="{{$b->gambar}}" class="col-md-6 order-md-last img image-popup js-fullheight d-flex align-items-center justify-content-center" style="background-image: url({{$b->gambar}});">
			<div class="icon d-flex align-items-center justify-content-center">
				<span class="fa fa-expand"></span>
			</div>
		</a>
		<div class="col-md-6">
			<div class="text pt-5 pl-0 px-lg-5 pl-md-4 ftco-animate">
				<div class="px-4 px-lg-4">
					<div class="desc">
						<div class="top">
							<span class="subheading">{{$b->tag}}</span>
							<h2 class="mb-4"><a href="gallery.html">{{$b->judul}}</a></h2>
						</div>
						<div class="absolute">
							<p>{{$b->isi}}</p>
						</div>
						<p><a href="/post/{{$b->id}}" class="custom-btn">View</a></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@else
<div class="col-md-12 portfolio-wrap">
	<div class="row no-gutters align-items-center">
		<a href="{{$b->gambar}}" class="col-md-6 img image-popup js-fullheight d-flex align-items-center justify-content-center" style="background-image: url({{$b->gambar}});">
			<div class="icon d-flex align-items-center justify-content-center">
				<span class="fa fa-expand"></span>
			</div>
		</a>
		<div class="col-md-6">
			<div class="text pt-5 pl-0 px-lg-5 pl-md-4 ftco-animate">
				<div class="px-4 px-lg-4">
					<div class="desc">
						<div class="top">
							<span class="subheading">{{$b->tag}}</span>
							<h2 class="mb-4"><a href="gallery.html">{{$b->judul}}</a></h2>
						</div>
						<div class="absolute">
							<p>{{$b->isi}}</p>
						</div>
						<p><a href="/post/{{$b->id}}" class="custom-btn">View</a></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endif

@endforeach

@endsection
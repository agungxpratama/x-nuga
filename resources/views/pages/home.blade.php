@extends('template.template')
@section('content')
<div class="col-lg-12 d-flex align-items-stretch">
    <div class="contact-wrap w-100 p-md-5 p-4">
		<h3 class="mb-4 heading">Home</h3>
	</div>
</div>

<div class="col-md-12 portfolio-wrap">
	<div class="row no-gutters align-items-center">
		<a href="{{asset('images/line_art.jpg')}}" class="col-md-6 order-md-last img image-popup js-fullheight d-flex align-items-center justify-content-center" style="background-image: url(images/line_art.jpg);">
			<div class="icon d-flex align-items-center justify-content-center">
				<span class="fa fa-expand"></span>
			</div>
		</a>
		<div class="col-md-6">
			<div class="text pt-5 pl-0 px-lg-5 pl-md-4 ftco-animate">
				<div class="px-4 px-lg-4">
					<div class="desc">
						<div class="top">
							<span class="subheading">Summary</span>
							<h2 class="mb-4"><a href="gallery.html">Profile</a></h2>
						</div>
						<div class="absolute">
							<p>Dear People, Nice to meet you. My name is Agung Septia Pratama, from Indonesia. a programmer and art enthusiasm, Posessing Diploma's in Aplied Science at Telkom University.</p>
						</div>
						<p><a href="single.html" class="custom-btn">View Portfolio</a></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="col-md-12 portfolio-wrap">
	<div class="row no-gutters align-items-center">
		<a href="{{asset('images/sun_pinch.jpg')}}" class="col-md-6 img image-popup js-fullheight d-flex align-items-center justify-content-center" style="background-image: url(images/sun_pinch.jpg);">
			<div class="icon d-flex align-items-center justify-content-center">
				<span class="fa fa-expand"></span>
			</div>
		</a>
		<div class="col-md-6">
			<div class="text pt-5 pl-0 px-lg-5 pl-md-4 ftco-animate">
				<div class="px-4 px-lg-4">
					<div class="desc">
						<div class="top">
							<span class="subheading">Activities</span>
							<h2 class="mb-4"><a href="gallery.html">Working Part Time &amp; Study</a></h2>
						</div>
						<div class="absolute">
							<p>Working as Freelancer and make something that suposed to be, and still folowing bachelor study at telkom university. fresh graduate at diploma.</p>
						</div>
						<p><a href="single.html" class="custom-btn">View Portfolio</a></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
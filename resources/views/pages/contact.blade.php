@extends('template.template')
@section('content')
<div class="col-lg-8 col-md-7 order-md-last d-flex align-items-stretch">
    <div class="contact-wrap w-100 p-md-5 p-4">
        <h3 class="mb-4 heading">Get in touch</h3>
        @if (session('message_feed'))
        <div class="alert alert-{{session('type')}} alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> {{ucfirst(session('type'))}}!</h4>
            {{session('message_feed')}}
          </div>
        @endif
        <form method="POST" id="contactForm" name="contactForm" class="contactForm" action="/feedback">
            @csrf
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="label" for="name">Full Name</label>
                        <input type="text" class="form-control" name="name" id="name" placeholder="Name">
                    </div>
                </div>
                <div class="col-md-6"> 
                    <div class="form-group">
                        <label class="label" for="email">Email Address</label>
                        <input type="email" class="form-control" name="email" id="email" placeholder="Email">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="label" for="subject">Subject</label>
                        <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="label" for="#">Message</label>
                        <textarea name="message" class="form-control" id="message" cols="30" rows="4" placeholder="Message"></textarea>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <input type="submit" value="Send Message" class="btn btn-primary">
                        <div class="submitting"></div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="col-lg-4 col-md-5 d-flex align-items-stretch">
    <div class="info-wrap js-fullheight bg-primary w-100 p-md-5 p-4">
        <h3>Let's get in touch</h3>
        <p class="mb-4">We're open for any suggestion or just to have a chat</p>
        <div class="dbox w-100 d-flex align-items-start">
            <div class="icon d-flex align-items-center justify-content-center">
                <span class="fa fa-map-marker"></span>
            </div>
            <div class="text pl-3">
                <p><span>Address:</span> Indonesia</p>
            </div>
        </div>
        <div class="dbox w-100 d-flex align-items-center">
            <div class="icon d-flex align-items-center justify-content-center">
                <span class="fa fa-phone"></span>
            </div>
            <div class="text pl-3">
                <p><span>Phone:</span> <a href="tel://1234567920">+6282282710200</a></p>
            </div>
        </div>
        <div class="dbox w-100 d-flex align-items-center">
            <div class="icon d-flex align-items-center justify-content-center">
                <span class="fa fa-paper-plane"></span>
            </div>
            <div class="text pl-3">
                <p><span>Email:</span> <a href="mailto:agungxpratama@gmail.com">agungxpratama@gmail.com</a></p>
            </div>
        </div>
        <div class="dbox w-100 d-flex align-items-center">
            <div class="icon d-flex align-items-center justify-content-center">
                <span class="fa fa-globe"></span>
            </div>
            <div class="text pl-3">
                <p><span>Website</span> <a href="#">x-nuga.my.id</a></p>
            </div>
        </div>
    </div>
</div>

@endsection
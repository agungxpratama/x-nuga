@extends('template.template')
@section('content')
<div id="colorlib-main">
    <section class="ftco-about img ftco-section ftco-no-pt ftco-no-pb" id="about-section">
        <div class="container-fluid px-0">
            <div class="row d-flex">
                <div class="col-md-6 d-flex">
                    <div class="img d-flex align-self-stretch align-items-center js-fullheight" style="background-image:url(images/about.jpg);">
                    </div>
                </div>
                <div class="col-md-6 d-flex align-items-center">
                    <div class="text px-4 pt-5 pt-md-0 px-md-4 pr-md-5 ftco-animate">
                        <h2 class="mb-4">Hello! I'm<span>Agung Pratama</span> the Developer of X-Nuga web</h2>
                        <p>Living in indonesia, it is a beautifull country. domestically in Bengkulu.</p>
                        {{-- <div class="team-wrap row mt-4">
                            <div class="col-md-4 team">
                                <div class="img" style="background-image: url(images/team-1.jpg);"></div>
                                <h3>John Doe</h3>
                                <span>Photographer</span>
                            </div>
                            <div class="col-md-4 team">
                                <div class="img" style="background-image: url(images/team-2.jpg);"></div>
                                <h3>John Flex</h3>
                                <span>Photographer</span>
                            </div>
                            <div class="col-md-4 team">
                                <div class="img" style="background-image: url(images/team-3.jpg);"></div>
                                <h3>John Flex</h3>
                                <span>Photographer</span>
                            </div>
                        </div> --}}
                    </div>
                    <div class="col-lg-12 d-flex align-items-stretch">
                        <div class="contact-wrap w-100 p-md-5 p-4">
                            <h3 class="mb-4 heading">About</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection
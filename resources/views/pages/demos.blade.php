@extends('template.template')
@section('content')
<div class="col-lg-12 d-flex align-items-stretch">
    <div class="contact-wrap w-100 p-md-5 p-4">
        <h3 class="mb-4 heading">List of Demo Apps</h3>
    </div>
</div>
<div class="col-md-4 portfolio-wrap-2">
    <div class="row no-gutters align-items-center">
        <div href="#" class="img w-100 h-100 d-flex align-items-center" style="background-image: url(images/apps/app_icon.png);">
            <div class="text p-4 p-md-5 ftco-animate">
                <div class="desc">
                    <div class="top">
                        <span class="subheading">Apps</span>
                        <h2 class="mb-4"><a href="single.html">E-Barn</a></h2>
                        <p><a href="" class="custom-btn">Visit</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-4 portfolio-wrap-2">
    <div class="row no-gutters align-items-center">
        <div href="#" class="img w-100 h-100 d-flex align-items-center" style="background-image: url(images/apps/coming-soon.png);">
            <div class="text p-4 p-md-5 ftco-animate">
                <div class="desc">
                    <div class="top">
                        <span class="subheading">--</span>
                        <h2 class="mb-4"><a href="single.html">Coming Soon</a></h2>
                        <p><a href="" class="custom-btn">...</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
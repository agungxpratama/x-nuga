@extends('template.template')
@section('content')
<div class="col-lg-12 d-flex align-items-stretch">
    <div class="contact-wrap w-100 p-md-5 p-4">
		<h3 class="mb-4 heading">Arts</h3>
	</div>
</div>
<div class="col-md-4 portfolio-wrap-2">
    <div class="row no-gutters align-items-center">
        @foreach ($arts as $a)
        <div href="#" class="img w-100 js-fullheight d-flex align-items-center" style="background-image: url({{$a->gambar}});">
            <div class="text p-4 p-md-5 ftco-animate">
                <div class="desc">
                    <div class="top bg-dark p-3">
                        <span class="subheading">{{$a->tag}}</span>
                        <h2 class="mb-4"><a href="single.html">{{$a->judul}}</a></h2>
                        <p><a href="single.html" class="custom-btn">View</a></p>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection
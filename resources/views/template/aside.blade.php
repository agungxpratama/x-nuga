<aside id="colorlib-aside" role="complementary" class="js-fullheight">

    <h1 id="colorlib-logo" class="mb-4 mb-md-5">
        <img width="25%" src="{{asset('images/logo.png')}}" alt=""><br>
        {{-- <br><a href="home" style="background-image: url(images/bg_1.jpg);">X-Nuga</a> --}}
    </h1>
    <nav id="colorlib-main-menu" role="navigation">
        <ul>
            <li class="{{ (request()->is('home')) ? 'colorlib-active' : '' }}"><a href="/home">Home</a></li>
            <li class="{{ (request()->is('arts')) ? 'colorlib-active' : '' }}"><a href="/arts">Arts</a></li>
            <li class="{{ (request()->is('blogs')) ? 'colorlib-active' : '' }}"><a href="/blogs">Blogs</a></li>
            <li class="{{ (request()->is('about')) ? 'colorlib-active' : '' }}"><a href="/about">About</a></li>
            <li class="{{ (request()->is('contact')) ? 'colorlib-active' : '' }}"><a href="/contact">Contact</a></li>
            <li class="{{ (request()->is('demos')) ? 'colorlib-active' : '' }}"><a href="/demos">Demos</a></li>
        </ul>
    </nav>

    <div class="colorlib-footer">
        <div class="mb-4">
            @if (session('message'))
            <div class="alert alert-{{session('type')}} alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {{session('message')}}
            </div>
            @endif

            <h3>Subscribe for newsletter</h3>
            <form action="/subscribe" class="colorlib-subscribe-form" method="POST">
                @csrf
                <div class="form-group d-flex">
                    <div class="icon"><span class="icon-paper-plane"></span></div>
                    <input type="email" class="form-control" placeholder="Enter Email Address" name="subscribe">
                </div>
            </form>
        </div>
        <p class="pfooter"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            {{-- Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib.com</a> --}}
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
    </div> 
</aside>
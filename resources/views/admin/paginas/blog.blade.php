@extends('admin.template.template')
@section('content')
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Blog</h1>
    {{-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> --}}
  </div>

  <!-- Content Row -->
  {{-- menampilkan error validasi --}}
  @if (count($errors) > 0)
  <div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <ul>
          @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
          @endforeach
      </ul>
  </div>
  @endif

  @if (session('message'))
  <div class="alert alert-{{session('type')}} alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      {{-- <h4 class="text-dark"><i class="icon fa fa-{{session('icon')}}"></i> {{ucfirst(session('type'))}}!</h4> --}}
      {{session('message')}}
  </div>
  @endif

  <div class="row">
    <!-- Pie Chart -->
    <div class="col-xl-4 col-lg-5">
      <div class="card border-top-secondary shadow mb-4">
        <!-- Card Header - Dropdown -->
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
          <h6 class="m-0 font-weight-bold text-primary">List Blogs</h6>
          <div class="dropdown no-arrow">
            <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
              <div class="dropdown-header">Dropdown Header:</div>
              <a class="dropdown-item" href="#">Action</a>
              <a class="dropdown-item" href="#">Another action</a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="#">Something else here</a>
            </div>
          </div>
        </div>
        <!-- Card Body -->
        <div class="card-body">
          @foreach ($blogs as $b)
          <div class="row">
            <div class="col-10">
              <a class="dropdown-item d-flex align-items-center" href="#">
                <div class="dropdown-list-image mr-3">
                  <img class="rounded-circle" src="{{$b->gambar}}" alt="" width="50em" height="50em">
                  <div class="status-indicator"></div>
                </div>
                <div>
                  <div class="text-truncate">{{$b->judul}}</div>
                  <div class="small text-gray-500">{{$b->author}} · {{$b->created_at}}</div>
                </div>
              </a>
            </div>
            <div class="col-2">
              <button class="btn btn-success btn-sm">Check</button>
              <a class="btn btn-danger btn-sm" href="/delete/{{$b->id}}">Delete</a>
            </div>
          </div>
          <div class="dropdown-divider"></div>
          @endforeach
          <a class="dropdown-item d-flex align-items-center" href="#">
            <div class="dropdown-list-image mr-3">
              <img class="rounded-circle" src="https://source.unsplash.com/AU4VPcFN4LE/60x60" alt="" width="50em" height="50em">
              <div class="status-indicator"></div>
            </div>
            <div>
              <div class="text-truncate">Compose??</div>
              <div class="small text-gray-500">See aside · to add post</div>
            </div>
          </a>
        </div>
      </div>
    </div>

    <!-- Area Chart -->
    <div class="col-xl-8 col-lg-7">
      {{-- <div class="card border-left-secondary shadow mb-4">
        <!-- Card Header - Dropdown -->
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
          <h6 class="m-0 font-weight-bold text-dark">Write Blog</h6>
          <div class="dropdown no-arrow">
            <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
              <div class="dropdown-header">Options:</div>
              <a class="dropdown-item" href="#">Setting</a>
              <a class="dropdown-item" href="#">Another action</a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="#">Hidden Post</a>
            </div>
          </div>
        </div>
        <!-- Card Body -->
        <div class="card-body">
            
        </div>
      </div> --}}
      <!-- Collapsable Card Example -->
      <div class="card border-left-secondary shadow mb-4">
        <!-- Card Header - Accordion -->
        <a href="#collapseCardExample" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseCardExample">
          <h6 class="m-0 font-weight-bold text-dark">Write Blog</h6>
        </a>
        <!-- Card Content - Collapse -->
        <div class="collapse show" id="collapseCardExample">
          <div class="card-body">
            <form method="POST" action="/upload" enctype="multipart/form-data">
              @csrf
              <div class="form-group row">
                <div class="col-sm-8 mb-3 mb-sm-0">
                  <input type="text" class="form-control" id="exampleFirstName" placeholder="Judul" name="judul">
                </div>
                <div class="col-sm-4">
                  {{-- <input type="text" class="form-control" id="exampleLastName" placeholder="Tag"> --}}
                  <div class="form-control bg-warning text-white"><i class="fas fa-calendar"></i> Blog</div>
                  <input type="hidden" name="tag" value="blog">
                </div>
              </div>
              <div class="form-group">
                <textarea name="isi" class="form-control" cols="30" rows="10" placeholder="Isi.."></textarea>
                {{-- <input type="email" class="form-control" id="exampleInputEmail" placeholder="Email Address"> --}}
              </div>
              <div class="form-group">
                <input type="file" class="form-control" name="gambar">
              </div>
              <hr>
              <input type="submit" class="btn btn-primary">
            </form>
          </div>
          <div class="card-footer">
            <button class="btn btn-primary"><i class="fas fa-envelope"></i> Post</button>
          </div>
        </div>
      </div>
    </div>
  </div>
    
@endsection
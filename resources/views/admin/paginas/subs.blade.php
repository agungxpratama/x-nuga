@extends('admin.template.template')
@section('content')

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Blog</h1>
    {{-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> --}}
</div>

<div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>No.</th>
              <th>Email</th>
              <th>Action</th>
            </tr>
          </thead>
          {{-- <tfoot>
            <tr>
              <th>Name</th>
              <th>Position</th>
              <th>Office</th>
              <th>Age</th>
              <th>Start date</th>
              <th>Salary</th>
            </tr>
          </tfoot> --}}
          <tbody>
              @php
                  $no = 0;
              @endphp
              @foreach ($subs as $s)
              @php
                  $no++;
              @endphp
              <tr>
                  <td>{{$no}}</td>
                  <td>{{$s->email}}</td>
                  <td>
                      <button class="btn btn-primary"><i class="fas fa-envelope mr-1"></i> Contact</button>
                      <button class="btn btn-danger"><i class="fas fa-trash mr-1"></i> Remove</button>
                  </td>
              </tr>
              @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>

@endsection
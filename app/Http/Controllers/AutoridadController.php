<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class AutoridadController extends Controller
{
    public function login()
    {
        return view('login');
    }

    public function login_(Request $request)
    {
        $check = User::where('username', $request->username)->first();
        if ($check) {
            if (Hash::check($request->password, $check->password)) {
                Session::put('username', $check->username);
                Session::put('email', $check->email);
                Session::put('login', True);
                $account = Account::where('email', $check->email)->first();
                // print_r($account);
                // print_r($check);
                if ($account) {
                    Session::put('name', $account->name);
                    Session::put('gambar', $account->gambar);
                }
                $request->session()->flash('login_message', 'Welcome Back');
                $request->session()->flash('type', 'success'); 
                $request->session()->flash('icon', 'check'); 
                return redirect('/casa');
            }else {
                $request->session()->flash('login_message', 'Sorry, Wrong Password');
                $request->session()->flash('type', 'warning'); 
                $request->session()->flash('icon', 'warning'); 
                return redirect('/login');
            }
        }else {
            $request->session()->flash('login_message', 'Sorry, Who Are you?');
            $request->session()->flash('type', 'danger'); 
            $request->session()->flash('icon', 'ban'); 
            return redirect('/login');
        }
        // echo Hash::make($request->password); 
    }

    public function logout()
    {
        Session::flush();
        return redirect('/home')->with('message','
        You have been successfully logged out!')->with('type', 'success');
    }
}

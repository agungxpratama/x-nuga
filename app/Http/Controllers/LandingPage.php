<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\Feedback;
use App\Models\Post;
use App\Models\Subscribe;
use Illuminate\Http\Request;

class LandingPage extends Controller
{
    public function Index()
    {
        return view('pages.home');
    }

    public function Contact()
    {
        return view('pages.contact');
    }

    public function feedback(Request $request)
    {
        $feedback = Feedback::create([
            'name' => $request->name,
            'email' => $request->email,
            'subject' => $request->subject,
            'message' => $request->message,
        ]);

        if ($feedback) {
            $request->session()->flash('message_feed', 'Sent');
            $request->session()->flash('type', 'success');
        }else{
            $request->session()->flash('message_feed', 'Youve Already subscribed');
            $request->session()->flash('type', 'warning');
        }
        return redirect('/contact');
    }

    public function Arts()
    {
        $arts = Post::where('tag', 'art')->get();
        return view('pages.arts', ['arts' => $arts]);
    }

    public function Blogs()
    {
        $blogs = Post::where('tag', 'blog')->get();
        return view('pages.blogs', ['blogs' => $blogs]);
    }

    public function Demos()
    {
        return view('pages.demos');
    }

    public function view($id)
    {
        $view = Post::findOrFail($id);
        print_r($view);
        // echo $view->author;
        $data = Account::where('name', $view->author)->first();
        print_r($data);
        if ($data) {
            return view('post.view', ['view' => $view, 'data' => $data]);
        }else {
            return view('post.view', ['view' => $view]);
        }
    }

    public function About()
    {
        return view('pages.about');
    }

    public function subscriptions(Request $request)
    {
        // echo $request->subscribe;
        // $subscriber = new Subscribe;
        // $subscriber->email = $request->subscriptions;
        // $subscriber->save();
        $status = Subscribe::where('email', $request->subscribe)->first();
        if (!$status) {
            $subscriber = Subscribe::create([
                'email' => $request->subscribe,
            ]);
            $request->session()->flash('message', 'Subscribed!!!');
            $request->session()->flash('type', 'success');
        }else{
            $request->session()->flash('message', 'Youve Already subscribed');
            $request->session()->flash('type', 'warning');
        }
        return redirect('/home');
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Feedback;
use App\Models\Post;
use App\Models\Subscribe;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use File;

class CasaController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if(!Session::get('login')){
                $request->session()->flash('login_message', 'You need to login First');
                $request->session()->flash('type', 'warning'); 
                return redirect('login');
            }
            return $next($request);
        });

    }

    public function index()
    {
        $blogs = Post::where('tag', 'blog')->count();
        $arte = Post::where('tag', 'art')->count();
        // echo $arte;
        $data = ['blog' => $blogs, 'arte' => $arte];
        return view('admin.paginas.casa', ['data' => $data]);
    }
    
    public function blogs()
    {
        $blogs = Post::where('tag', 'blog')->get();
        return view('admin.paginas.blog', ['blogs' => $blogs]);
    }

    public function arte()
    {
        return view('admin.paginas.arte');
    }

    public function upload(Request $request)
    {
        $this->validate($request, [
             'judul' => 'required',
             'isi' => 'required',
             'tag' => 'required',
             'gambar' => 'required',
             ]);
        $gambar = $request->file('gambar');
        $upload = Post::create([
        // $data = [
            'judul' => $request->judul,
            'isi' => $request->isi,
            'tag' => $request->tag,
            'author' => session('name'),
            'gambar' => 'images/post/'.$request->tag.'/'.time().'.'.$gambar->getClientOriginalExtension(),
        // ];
        ]);
        if ($upload) {
            $gambar->move('images/post/'.$request->tag.'/', time().'.'.$gambar->getClientOriginalExtension());
            $request->session()->flash('message', 'Berhasil Upload');
            $request->session()->flash('type', 'success');
            return redirect('/blog');
        }
        // print_r($data);
    }

    public function delete(Request $request, $id)
    {
        $post = Post::where('id', $id)->first();
        $tag = $post->tag;
        // Storage::delete($post->gambar);
        // $deletePath = Storage::delete('public/'.$post->gambar);
        // print_r($tag);
        // echo $tag->tag;
        $delete = Post::where('id', $id)->delete();
        if ($delete) {
            if(File::exists(public_path($post->gambar))){
                File::delete(public_path($post->gambar));
                $request->session()->flash('message', 'File deleted.');
                $request->session()->flash('type', 'warning');
            }else{
                $request->session()->flash('message', 'File does not exist');
                $request->session()->flash('type', 'danger');
            }
        }
        return redirect('/'.$tag);
    }

    public function subs()
    {
        $subs = Subscribe::all();
        return view('admin.paginas.subs', ['subs' => $subs]);
    }

    public function feedback()
    {
        $feedback = Feedback::all();
        return view('admin.paginas.feedback', ['feedback' => $feedback]);
    }
}

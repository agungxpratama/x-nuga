<?php

use App\Http\Controllers\AutoridadController;
use App\Http\Controllers\CasaController;
use App\Http\Controllers\LandingPage;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', [LandingPage::class, 'Index'])->name('home');
Route::get('/about', [LandingPage::class, 'About']);
Route::get('/contact', [LandingPage::class, 'Contact']);
Route::post('/feedback', [LandingPage::class, 'feedback']);
Route::get('/demos', [LandingPage::class, 'Demos']);
Route::get('/arts', [LandingPage::class, 'Arts']);
Route::get('/blogs', [LandingPage::class, 'Blogs']);
Route::get('/post/{id}', [LandingPage::class, 'view']);
// Route::get('/post', [LandingPage::class, 'view']);

Route::get('/login', [AutoridadController::class, 'login']);
Route::get('/logout', [AutoridadController::class, 'logout']);
Route::post('/login_', [AutoridadController::class, 'login_']);


Route::post('/subscribe', [LandingPage::class, 'subscriptions']);

Route::get('/casa', [CasaController::class, 'index']);
Route::get('/blog', [CasaController::class, 'blogs']);
Route::get('/arte', [CasaController::class, 'arte']);
Route::post('/upload', [CasaController::class, 'upload']);
Route::get('/delete/{id}', [CasaController::class, 'delete']);
Route::get('/subs', [CasaController::class, 'subs']);
Route::get('/feedback', [CasaController::class, 'feedback']);

